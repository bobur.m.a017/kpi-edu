FROM openjdk:17-alpine3.14
EXPOSE 2266
ADD target/kpi.jar kpi.jar
ENTRYPOINT ["java","-jar","kpi.jar"]