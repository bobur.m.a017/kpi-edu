package uz.edu.demokpi.dto;

public class StateMessage {
    private String text;
    private boolean success;
    private Object object;

    public StateMessage(String text, boolean success, Object object) {
        this.text = text;
        this.success = success;
        this.object = object;
    }

    public StateMessage(String text, boolean success) {
        this.text = text;
        this.success = success;
    }

    public StateMessage() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
