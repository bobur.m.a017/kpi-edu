package uz.edu.demokpi.dto;

import uz.edu.demokpi.report.UserAndReports;
import uz.edu.demokpi.user.Users;

import java.util.List;

public class DTO {

    private Object category;
    private List<Object> list;
    private List<Object> users;


    public DTO(Object category, List<Object> list) {
        this.category = category;
        this.list = list;
    }

    public DTO() {
    }

    public List<Object> getUsers() {
        return users;
    }

    public void setUsers(List<Object> users) {
        this.users = users;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }
}
