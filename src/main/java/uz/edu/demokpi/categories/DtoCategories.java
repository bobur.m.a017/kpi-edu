package uz.edu.demokpi.categories;

public class DtoCategories {
    private String name;
    private Integer parentId;

    public DtoCategories(String name, Integer categoryId) {
        this.name = name;

        this.parentId = categoryId;
    }

    public DtoCategories() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }
}
