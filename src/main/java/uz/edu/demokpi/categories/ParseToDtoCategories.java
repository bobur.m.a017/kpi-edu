package uz.edu.demokpi.categories;

import java.util.List;

public interface ParseToDtoCategories {
    default CategoriesResponseDto pareCategories(Categories categories){
        return new CategoriesResponseDto(categories.getId(),categories.getName(),categories.getTypeName(),categories.getParentId());
    }
    default Integer categoriesId(List<Categories> categories, Integer categoryId){
        for (Categories category : categories) {
            if (category.getId().equals(categoryId)){
                return category.getId();
            }
        }

        return null;

    }
}
