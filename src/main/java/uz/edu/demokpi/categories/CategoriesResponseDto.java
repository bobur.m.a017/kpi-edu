
package uz.edu.demokpi.categories;


public class CategoriesResponseDto {
    private Integer id;
    private String name;
    private String typeName;
    private Integer parentId;

    public CategoriesResponseDto() {
    }

    public CategoriesResponseDto(Integer id, String name, String typeName, Integer parentId) {
        this.id = id;
        this.name = name;
        this.typeName = typeName;
        this.parentId = parentId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
