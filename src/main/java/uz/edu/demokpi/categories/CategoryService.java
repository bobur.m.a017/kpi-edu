package uz.edu.demokpi.categories;

import org.springframework.stereotype.Service;
import uz.edu.demokpi.dto.DTO;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.user.Users;
import uz.edu.demokpi.user.UsersRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final UsersRepository usersRepository;

    public CategoryService(CategoryRepository categoryRepository, UsersRepository usersRepository) {
        this.categoryRepository = categoryRepository;
        this.usersRepository = usersRepository;
    }

    public Categories getOneCategory(int categoryId) {
        Optional<Categories> optionalCategories = categoryRepository.findById(categoryId);
        return optionalCategories.get();
    }

    public List<Categories> getSubCategory(Object categoryId) {

        List<Categories> allByParentId = null;

        try {
            allByParentId = categoryRepository.findAllByParentId(Integer.parseInt((String) categoryId));
        } catch (Exception e) {
            allByParentId = categoryRepository.findByTypeName((String) categoryId);
        }


        return allByParentId;
    }

    public List<Categories> getAllCategory() {
        return categoryRepository.findAll();
    }

    public Categories getAllCategorybYnAME(Integer id) {
        return categoryRepository.findById(id).get();
    }

    public StateMessage deleteCategory(Integer id) {

        try {

            if (categoryRepository.existsByParentId(id)) {
                Boolean aBoolean = categoryRepository.deleteAllByParentId(id);
            }
            categoryRepository.deleteById(id);
            return new StateMessage("O'chirildi", true);
        } catch (Exception e) {
            return new StateMessage("O`chirilmadi, ushbu " + categoryRepository.findById(id).get().getTypeName() + "ga xodim biriktirilgan", false);
        }
    }

    public StateMessage editCategory(Integer id, DtoCategories dtoCategories) {

        Categories categories = categoryRepository.findById(id).get();
        List<Integer> usersId = new ArrayList<>(categories.getUsersId());


        if (dtoCategories.getName() != null) {
            categories.setName(dtoCategories.getName());
        }
        if (dtoCategories.getParentId() != null) {
            if (categories.getParentId() != 0) {

                for (int i = 0; i < usersId.size(); i++) {

                    addUsersId(usersRepository.findById(usersId.get(i)).get(), false);
                }
                categories.setParentId(dtoCategories.getParentId());
                categories.setParentName(categoryRepository.findById(dtoCategories.getParentId()).get().getName());
                categoryRepository.save(categories);

                for (int i = 0; i < usersId.size(); i++) {
                    addUsersId(usersRepository.findById(usersId.get(i)).get(), true);
                    if (i == usersId.size()-1){
                        break;
                    }
                }
            }
        }
        categoryRepository.save(categories);

        return new StateMessage("O'zgartirildi", true);
    }

    public StateMessage addCategories(String name, Integer id) {
        String typeName = "";

        if (id == null) {
            id = 0;
            typeName = TypeCategory.MANAGEMENT.getTypeName();
        } else {
            Optional<Categories> parentCategory = categoryRepository.findById(id);
            if (parentCategory.get().getTypeName().equals(TypeCategory.MANAGEMENT.getTypeName())) {
                typeName = TypeCategory.BOARDS.getTypeName();
            } else if (parentCategory.get().getTypeName().equals(TypeCategory.BOARDS.getTypeName())) {
                typeName = TypeCategory.SECTION.getTypeName();
            }
        }

        categoryRepository.save(new Categories(name, typeName, id, id != 0 ? categoryRepository.findById(id).get().getName() : ""));
        return new StateMessage("Qo'shildi", true);
    }

    public void addUsersId(Users users, boolean res) {

        boolean stop = true;
        Categories categories = users.getCategories();
        while (stop) {
            if (res) {
                categories.getUsersId().add(users.getId());
                categoryRepository.save(categories);

                Optional<Categories> byId = categoryRepository.findById(categories.getParentId());

                if (byId.isEmpty()) {
                    stop = false;
                } else {
                    categories = byId.get();
                }
            } else {
                categories.getUsersId().remove(users.getId());
                categoryRepository.save(categories);

                Optional<Categories> byId = categoryRepository.findById(categories.getParentId());

                if (byId.isEmpty()) {
                    stop = false;
                } else {
                    categories = byId.get();
                }
            }
        }
    }

    public List<DTO> getCategory(Integer categoryId) {
        return null;
    }

    public void editCategoriesUserId(Categories categories, Categories newCategory) {

        removeUserIdCategory(categoryRepository.findById(categories.getParentId()).get(), categories.getUsersId());
        addUserIdCategory(newCategory, categories.getUsersId());

    }

    public void addUserIdCategory(Categories categories, List<Integer> usersId) {

        while (true) {

            categories.getUsersId().addAll(usersId);
            categoryRepository.save(categories);
            if (categories.getParentId() != 0) {
                categories = categoryRepository.findById(categories.getParentId()).get();
            } else {
                break;
            }
        }
    }

    public void removeUserIdCategory(Categories categories, List<Integer> usersId) {

        while (true) {
            for (Integer userId : categories.getUsersId()) {
                usersId.removeIf(id -> Objects.equals(id, userId));
            }
            categoryRepository.save(categories);
            if (categories.getParentId() != 0) {
                categories = categoryRepository.findById(categories.getParentId()).get();
            } else {
                break;
            }
        }

    }


    public void edit(Categories parentCategories, Categories subCategories, boolean res) {

        if (!res) {

            for (Integer userId : parentCategories.getUsersId()) {

                for (Integer id : subCategories.getUsersId()) {
                    if (Objects.equals(userId, id)) {

                        parentCategories.getUsersId().remove(id);
                        categoryRepository.save(parentCategories);

                    }
                }

            }
        }


    }

}
