package uz.edu.demokpi.categories;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.edu.demokpi.report.Report;
import uz.edu.demokpi.user.Users;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Categories,Integer> {
    Boolean deleteAllByParentId(Integer id);
    List<Categories> findByTypeName(String name);
    boolean existsByParentId(Integer id);
    Optional<Categories> findByParentId(Integer integer);
    List<Categories> findAllByParentId(Integer id);
}
