package uz.edu.demokpi.categories;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryReportRepository extends JpaRepository<CategoryReport,Integer> {


   Optional<CategoryReport> findByCategoriesAndYearAndMonth(Categories categories, Integer year, Integer month);

}
