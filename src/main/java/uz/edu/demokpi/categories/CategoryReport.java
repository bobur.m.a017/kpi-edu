package uz.edu.demokpi.categories;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import uz.edu.demokpi.report.UserTasks;
import uz.edu.demokpi.user.Users;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;

@Entity
public class CategoryReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer year = Year.now().getValue();
    private Integer month = YearMonth.now().getMonthValue();
    private Integer generalBall;
    private Integer additionalPayment;
    private Integer plan;
    private Integer implementation;
    private Integer countUsers;

    @OneToMany
    private List<UserTasks> userTasks;


    @UpdateTimestamp
    private Timestamp updatedDate;
    @CreationTimestamp
    private Timestamp createdDate;

    @JsonIgnore
    @ManyToOne
    private Categories categories;


    public List<UserTasks> getUserTasks() {
        return userTasks;
    }

    public void setUserTasks(List<UserTasks> userTasks) {
        this.userTasks = userTasks;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCountUsers() {
        return countUsers;
    }

    public void setCountUsers(Integer countUsers) {
        this.countUsers = countUsers;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getGeneralBall() {
        return generalBall;
    }

    public void setGeneralBall(Integer generalBall) {
        this.generalBall = generalBall;
    }

    public Integer getAdditionalPayment() {
        return additionalPayment;
    }

    public void setAdditionalPayment(Integer additionalPayment) {
        this.additionalPayment = additionalPayment;
    }

    public Integer getPlan() {
        return plan;
    }

    public void setPlan(Integer plan) {
        this.plan = plan;
    }

    public Integer getImplementation() {
        return implementation;
    }

    public void setImplementation(Integer implementation) {
        this.implementation = implementation;
    }

    public Integer getPercentage() {
        if (implementation != null && plan != null && plan != 0 && implementation != 0) {
            return (implementation * 100) / plan;
        }
        return 0;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }
}
