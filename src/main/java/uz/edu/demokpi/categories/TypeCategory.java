package uz.edu.demokpi.categories;

public enum TypeCategory {

    MANAGEMENT("Rahbariyat",1),
    BOARDS("Boshqarma",2),
    SECTION("Bo'lim",3);


    private String typeName;
    private Integer id;


    TypeCategory(String typeName, Integer id) {
        this.typeName = typeName;
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public Integer getId() {
        return id;
    }
}
