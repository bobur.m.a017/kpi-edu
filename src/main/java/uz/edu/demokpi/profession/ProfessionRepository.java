package uz.edu.demokpi.profession;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.edu.demokpi.role.Role;

import java.util.Optional;

public interface ProfessionRepository extends JpaRepository<Profession, Integer> {
    Optional<Profession> findByName(String name);

    boolean existsByName(String s);
}
