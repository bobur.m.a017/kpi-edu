package uz.edu.demokpi.profession;

import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;
import uz.edu.demokpi.dto.StateMessage;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessionService {
    private final ProfessionRepository professionRepository;

    public ProfessionService(ProfessionRepository professionRepository) {
        this.professionRepository = professionRepository;
    }


    public Profession getOneProfession(int professionId) {
        return professionRepository.getById(professionId);
    }

    public List<Profession> getAllProfessions() {
        return professionRepository.findAll();
    }

    public StateMessage deleteProfession(int professionId) {
        try {
            professionRepository.deleteById(professionId);
            return new StateMessage("O'chirildi", true);
        } catch (Exception exception) {
            return new StateMessage("Lavozim o`chirilmadi, ushbu lavozim xodimga biriktirilgan", false);
        }
    }

    public StateMessage editProfession(ProfessionDTO professionDTO, Integer id) {
        Optional<Profession> optionalProfession = professionRepository.findById(id);
        if (optionalProfession.isPresent()) {
            Profession profession = optionalProfession.get();
            profession.setName(professionDTO.getName());
            Profession savedPro = professionRepository.save(profession);
            return new StateMessage("Qo`shildi", true, savedPro);
        } else return new StateMessage("failed", false, null);
    }

    public StateMessage addProfession(String name) {
        professionRepository.save(new Profession(name));
        return new StateMessage("Qo'shildi", true);
    }
}
