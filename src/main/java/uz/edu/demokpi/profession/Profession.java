package uz.edu.demokpi.profession;

import javax.persistence.*;

@Entity
public class Profession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String name;


    public Profession(String name, Integer percentage) {
        this.name = name;
    }



    public Profession(String name) {
        this.name = name;
    }

    public Profession() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
