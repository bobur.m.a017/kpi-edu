package uz.edu.demokpi.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.edu.demokpi.user.ResponsUser;
import uz.edu.demokpi.user.SignInUser;
import uz.edu.demokpi.user.Users;

import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Service
public class ApplicationUsernamePasswordAuthenticationFilter {

    private final PasswordEncoder passwordEncoder;
    private final ByUserDetailes byUserDetailes;
    private final AuthenticationManager authenticationManager;

    private final String secretKey = "HJAtS9ALVpOPTCXw5W0Ifx2sHcBwmTKNKaqTgwD4";


    public ApplicationUsernamePasswordAuthenticationFilter(PasswordEncoder passwordEncoder,
                                                           ByUserDetailes byUserDetailes, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.byUserDetailes = byUserDetailes;
        this.authenticationManager = authenticationManager;
    }
    private final SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));

    private long exoaredDate = 86400000L * 10;

    public ResponsUser successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException, ServletException {

        ObjectMapper objectMapper = new ObjectMapper();
        SignInUser adminLogin
                = objectMapper.readValue(request.getInputStream(), SignInUser.class);

        Users user;
        try {
           Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(adminLogin.getLogin(), adminLogin.getPassword()));
           user = (Users) authenticate.getPrincipal();

        String token = Jwts.builder()
                .signWith(key,SignatureAlgorithm.HS256)
                .claim("roles",user.getRole().getName())
                .setSubject(user.getUsername())// <---
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + exoaredDate)) // 1 kun
                .compact();

            token = "Bearer " + token;
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(user,
                        null,
                        user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        return new ResponsUser(user.getId(),user.getRole().getName(),token,true,user.getUsername());
        }catch (Exception e){
            return new ResponsUser(0,null,null,false,"Login yoki parol xato qayta uruning!!");
        }
    }

    private Claims getClaims(String token) {
        Claims claims;
        try {
            claims = Jwts.parserBuilder()
                    .setSigningKey(key).build() // Set the identification name
                    .parseClaimsJws(token)  // Analytical TOKEN
                    .getBody();
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }
        return claims;

    }

    public ResponsUser parseToken2(HttpServletRequest request){

        String role = request.getHeader("role");
        int id = Integer.parseInt(request.getHeader("id"));
        ResponsUser responsUser=new ResponsUser();
        responsUser.setId(id);
        responsUser.setRole(role);
        return responsUser;
    }
    public ResponsUser parseToken(HttpServletRequest httpServletRequest){

        String bearerToken = httpServletRequest.getHeader("Authorization");

        String token = bearerToken.replace("Bearer ", "");


        Claims claims;
        try {
            claims = Jwts.parserBuilder()
                    .setSigningKey(key).build() // Set the identification name
                    .parseClaimsJws(token)  // Analytical TOKEN
                    .getBody();
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        }

        String username = claims.getSubject();


        String role =(String) claims.get("roles");
        ResponsUser responsUser=new ResponsUser();
        responsUser.setUsername(username);
        responsUser.setRole(role);
        return responsUser;
    }

    private Jws<Claims> getClaims2(String token) {

        Jws<Claims> jws = null;
        try {
            jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
        } catch (JwtException ignored) {

        }
        return jws;

    }





}
