package uz.edu.demokpi.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.edu.demokpi.user.Users;
import uz.edu.demokpi.user.UsersRepository;

import java.util.Optional;

@Service
public class ByUserDetailes implements UserDetailsService {

   private final UsersRepository usersRepository;

    public ByUserDetailes(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> byUsername = usersRepository.findByUsername(username);
        return byUsername.orElse(null);
    }
}
