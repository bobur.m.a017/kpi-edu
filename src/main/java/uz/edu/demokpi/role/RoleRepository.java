package uz.edu.demokpi.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;



@RepositoryRestResource(path = "role")
public interface RoleRepository extends JpaRepository<Role,Integer> {
    Optional<Role> findByName(String name);
    boolean existsByName(String name);
    boolean existsRoleByName(String name);
}
