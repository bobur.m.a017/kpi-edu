package uz.edu.demokpi.role;

import org.springframework.stereotype.Service;
import uz.edu.demokpi.dto.StateMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    public StateMessage add(DtoRequestRole dtoRequestRole){
        Role role=null;
        if (dtoRequestRole.getId()!=null){
            Optional<Role> byId = roleRepository.findById(dtoRequestRole.getId());
            if (byId.isEmpty()){
                return new StateMessage("Xatolik",false);
            }else {
                role = byId.get();
                role.setName(dtoRequestRole.getName());

                return  new StateMessage("Bajarildi",true);
            }
        }else {
            roleRepository.save(
                    new Role(
                            dtoRequestRole.getName()
                    )
            );
          return   new StateMessage("Bajarildi",true);
        }

    }
    public StateMessage deleteRole(Integer id){
        roleRepository.deleteById(id);
        return new StateMessage("Bajarildi",true);
    }
    public List<DtoRequestRole> getAllRole(){
        List<Role> all = roleRepository.findAll();
        List<DtoRequestRole> dtoRole=new ArrayList<>();
        for (Role role : all) {
            dtoRole.add(
                    new DtoRequestRole(
                            role.getId(),
                            role.getName()
                    )
            );
        }

        return dtoRole;
    }

}
