package uz.edu.demokpi.task;

import javax.persistence.*;

@Entity
public class Tasks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String name;
    private Integer maxBall;
    private Integer minBall;
    private Integer roleId;
    private String roleName;
    private boolean type;

    public Tasks(String name, Integer maxBall, Integer minBall, Integer roleId,boolean type) {
        this.name = name;
        this.maxBall = maxBall;
        this.minBall = minBall;
        this.roleId = roleId;
        this.type = type;
    }

    public Tasks( String name, Integer maxBall, Integer minBall, Integer roleId, String roleName, boolean type) {

        this.name = name;
        this.maxBall = maxBall;
        this.minBall = minBall;
        this.roleId = roleId;
        this.roleName = roleName;
        this.type = type;
    }

    public Tasks() {
    }


    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxBall() {
        return maxBall;
    }

    public void setMaxBall(Integer maxBall) {
        this.maxBall = maxBall;
    }

    public Integer getMinBall() {
        return minBall;
    }

    public void setMinBall(Integer minBall) {
        this.minBall = minBall;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
