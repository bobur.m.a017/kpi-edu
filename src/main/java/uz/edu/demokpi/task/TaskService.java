package uz.edu.demokpi.task;


import org.springframework.stereotype.Service;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.role.RoleRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final RoleRepository roleRepository;


    public TaskService(TaskRepository taskRepository, RoleRepository roleRepository) {
        this.taskRepository = taskRepository;
        this.roleRepository = roleRepository;
    }


    public StateMessage add(TaskDTO taskDTO) {

        Tasks tasks = new Tasks(taskDTO.getName(), taskDTO.getMaxBall(), taskDTO.getMinBall(), taskDTO.getRoleId(),roleRepository.findById(taskDTO.getRoleId()).get().getName(), taskDTO.isType());
        taskRepository.save(tasks);

        return new StateMessage("Muvafiaqatli qo`shildi", true);
    }

    public StateMessage delete(Integer id) {

        taskRepository.deleteById(id);

        return new StateMessage("Muvafiaqatli qo`shildi", true);
    }

    public StateMessage edit(TaskDTO taskDTO, Integer id) {
        Optional<Tasks> byId = taskRepository.findById(id);

        if (byId.isPresent()) {
            Tasks tasks = byId.get();
            tasks.setMaxBall(taskDTO.getMaxBall());
            tasks.setMinBall(taskDTO.getMinBall());
            tasks.setName(taskDTO.getName());
            tasks.setRoleId(taskDTO.getRoleId());
            tasks.setRoleName(roleRepository.findById(taskDTO.getRoleId()).get().getName());
            tasks.setType(taskDTO.isType());
            taskRepository.save(tasks);
            return new StateMessage("Muvafiaqatli o`zgartirildi", true);
        }
        return new StateMessage("Xatolik o`zgartirilmadi", true);
    }

    public List<Tasks> getAll() {
        return taskRepository.findAll();
    }

    public Tasks get(Integer id) {
        return taskRepository.findById(id).get();
    }
}
