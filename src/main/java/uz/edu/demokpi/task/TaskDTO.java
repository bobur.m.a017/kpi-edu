package uz.edu.demokpi.task;

import io.swagger.models.auth.In;

import java.util.Iterator;

public class TaskDTO {

    private String name;
    private Integer maxBall;
    private Integer minBall;
    private Integer roleId;
    private boolean type;


    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxBall() {
        return maxBall;
    }

    public void setMaxBall(Integer maxBall) {
        this.maxBall = maxBall;
    }

    public Integer getMinBall() {
        return minBall;
    }

    public void setMinBall(Integer minBall) {
        this.minBall = minBall;
    }
}
