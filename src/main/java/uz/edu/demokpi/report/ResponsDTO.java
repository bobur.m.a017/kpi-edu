package uz.edu.demokpi.report;

import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.categories.CategoriesResponseDto;
import uz.edu.demokpi.categories.CategoryReport;
import uz.edu.demokpi.user.DtoRequest;
import uz.edu.demokpi.user.Users;

import java.util.List;

public class ResponsDTO {
    private CategoryReport categoryReport;
    private CategoriesResponseDto category;
    private DtoRequest leader;
    private List<DtoRequest> users;


    public ResponsDTO(CategoriesResponseDto category, DtoRequest leader, List<DtoRequest> users) {
        this.category = category;
        this.leader = leader;
        this.users = users;
    }

    public ResponsDTO(CategoryReport categoryReport, CategoriesResponseDto category, DtoRequest leader, List<DtoRequest> users) {
        this.categoryReport = categoryReport;
        this.category = category;
        this.leader = leader;
        this.users = users;
    }

    public ResponsDTO() {
    }

    public CategoryReport getCategoryReport() {
        return categoryReport;
    }

    public void setCategoryReport(CategoryReport categoryReport) {
        this.categoryReport = categoryReport;
    }

    public CategoriesResponseDto getCategory() {
        return category;
    }

    public void setCategory(CategoriesResponseDto category) {
        this.category = category;
    }

    public DtoRequest getLeader() {
        return leader;
    }

    public void setLeader(DtoRequest leader) {
        this.leader = leader;
    }

    public List<DtoRequest> getUsers() {
        return users;
    }

    public void setUsers(List<DtoRequest> users) {
        this.users = users;
    }
}