package uz.edu.demokpi.report;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.time.Year;
import java.time.YearMonth;

@Entity
public class UserTasks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer taskId;
    private String name;
    private Integer score;
    private Integer maxBall;
    private Integer minBall;
    private Float percentage;
    private Integer year= Year.now().getValue();
    private Integer month= YearMonth.now().getMonthValue();
    private Integer roleId;
    private String roleName;
    private boolean type;
    private Integer plan;
    private Integer implementation;

    @UpdateTimestamp
    private Timestamp updatedDate;
    @CreationTimestamp
    private Timestamp createdDate;



    public UserTasks() {

    }



    public UserTasks(Integer taskId, String name, Integer score,Integer maxBall,Integer minBall, Float percentage) {
        this.taskId = taskId;
        this.name = name;
        this.score = score;
        this.minBall = minBall;
        this.maxBall = maxBall;
        this.percentage = percentage;
    }

    public UserTasks(Integer taskId, String name, Integer score, Integer maxBall, Integer minBall, Float percentage, Integer roleId, String roleName) {
        this.taskId = taskId;
        this.name = name;
        this.score = score;
        this.maxBall = maxBall;
        this.minBall = minBall;
        this.percentage = percentage;
        this.roleId = roleId;
        this.roleName = roleName;

    }
    public UserTasks(Integer taskId, String name, Integer score, Integer maxBall, Integer minBall, Integer roleId, String roleName) {
        this.taskId = taskId;
        this.name = name;
        this.score = score;
        this.maxBall = maxBall;
        this.minBall = minBall;
        this.roleId = roleId;
        this.roleName = roleName;

    }

    public Integer getPlan() {
        return plan;
    }

    public void setPlan(Integer plan) {
        this.plan = plan;
    }

    public Integer getImplementation() {
        return implementation;
    }

    public void setImplementation(Integer implementation) {
        this.implementation = implementation;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer role) {
        this.roleId = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getMaxBall() {
        return maxBall;
    }

    public void setMaxBall(Integer maxBall) {
        this.maxBall = maxBall;
    }

    public Integer getMinBall() {
        return minBall;
    }

    public void setMinBall(Integer minBall) {
        this.minBall = minBall;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
