package uz.edu.demokpi.report;

import uz.edu.demokpi.task.TaskRepository;
import uz.edu.demokpi.task.Tasks;

import java.util.ArrayList;
import java.util.List;


public abstract class AddNullReports {


    Report addNewReports(List<Tasks> tasks) {

        List<UserTasks> userTasks = new ArrayList<>();

        for (Tasks task : tasks) {
            UserTasks userTasks1 = new UserTasks(task.getId(), task.getName(), 0, task.getMaxBall(), task.getMinBall(), task.getRoleId(), task.getRoleName());

            if (task.isType()){
                userTasks1.setImplementation(0);
                userTasks1.setPlan(0);
                userTasks1.setPercentage((float)0);
            }
            userTasks.add(userTasks1);
        }
        return new Report(0, 0, userTasks);
    }
}