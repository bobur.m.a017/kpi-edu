package uz.edu.demokpi.report;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.edu.demokpi.categories.CategoryReport;
import uz.edu.demokpi.user.Users;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface ReportRepository extends JpaRepository<Report,Integer> {
    Optional<Report> findByUsers_IdAndYearAndMonth(Integer userId, Integer year, Integer month);
    List<Report> findAllByCategoryReportAndYearAndMonth(CategoryReport categoryReport,Integer year,Integer month);


    List<Report> findAllByUsers(Users users);
    List<Report>findAllByYearAndMonth(Integer year, Integer month);
    List<Report>findAllByYear(Integer year);
    List<Report>findAllByUsers_Id(Integer id);

    @Query("SELECT u FROM Report u WHERE u.createdDate > ?1 and u.createdDate < ?2")
    List<Report>findAllByDate(Timestamp start,Timestamp and);


}
