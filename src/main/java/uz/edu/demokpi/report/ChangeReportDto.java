package uz.edu.demokpi.report;


import java.time.Year;
import java.time.YearMonth;

public class ChangeReportDto {

    private Integer categoryId;
    private Integer year = Year.now().getValue();
    private Integer month = YearMonth.now().getMonthValue();
    private boolean status;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
