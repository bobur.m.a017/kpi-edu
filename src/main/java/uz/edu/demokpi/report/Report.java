package uz.edu.demokpi.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import uz.edu.demokpi.categories.CategoryReport;
import uz.edu.demokpi.user.Users;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;

@Entity
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer year= Year.now().getValue();
    private Integer month= YearMonth.now().getMonthValue();
    private Integer generalBall;
    private Integer additionalPayment;
    private boolean status =true;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<UserTasks> userTasks;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Users users;
    @UpdateTimestamp
    private Timestamp updatedDate;
    @CreationTimestamp
    private Timestamp createdDate;

    @ManyToOne
    private CategoryReport categoryReport;




    public Report() {
    }

    public Report(Integer generalBall, Integer additionalPayment, List<UserTasks> userTasks) {
        this.generalBall = generalBall;
        this.additionalPayment = additionalPayment;
        this.userTasks = userTasks;
    }

    public Report(Integer generalBall, Integer additionalPayment, List<UserTasks> userTasks, Users users, boolean status) {
        this.generalBall = generalBall;
        this.additionalPayment = additionalPayment;
        this.userTasks = userTasks;
        this.users = users;
        this.status = status;
    }

    public CategoryReport getCategoryReport() {
        return categoryReport;
    }

    public void setCategoryReport(CategoryReport categoryReport) {
        this.categoryReport = categoryReport;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getGeneralBall() {
        return generalBall;
    }

    public void setGeneralBall(Integer generalBall) {
        this.generalBall = generalBall;
    }

    public Integer getAdditionalPayment() {
        return additionalPayment;
    }

    public void setAdditionalPayment(Integer additionalPayment) {
        this.additionalPayment = additionalPayment;
    }

    public List<UserTasks> getUserTasks() {
        return userTasks;
    }

    public void setUserTasks(List<UserTasks> userTasks) {
        this.userTasks = userTasks;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
