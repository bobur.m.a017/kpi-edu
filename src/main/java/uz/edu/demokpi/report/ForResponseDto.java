package uz.edu.demokpi.report;

import io.swagger.models.auth.In;

public class ForResponseDto {

    private Integer taskId;
    private String value;
    private Integer plan;
    private Integer implementation;

    public ForResponseDto(Integer taskId, String value, String name) {
        this.taskId = taskId;
        this.value = value;
    }

    public ForResponseDto() {
    }


    public Integer getPlan() {
        return plan;
    }

    public void setPlan(Integer plan) {
        this.plan = plan;
    }

    public Integer getImplementation() {
        return implementation;
    }

    public void setImplementation(Integer implementation) {
        this.implementation = implementation;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
