package uz.edu.demokpi.report;

import org.springframework.data.rest.core.config.Projection;
import uz.edu.demokpi.task.Tasks;

@Projection(types = Tasks.class)
public interface CustomTasks {
    Integer getId();
    String getName();
    Integer getMaxBall();
    Integer getMinBall();
    Boolean getType();
}
