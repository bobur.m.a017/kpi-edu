package uz.edu.demokpi.report;

import org.springframework.stereotype.Service;
import uz.edu.demokpi.role.Role;
import uz.edu.demokpi.role.RoleRepository;
import uz.edu.demokpi.task.TaskRepository;
import uz.edu.demokpi.task.Tasks;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserTaskService {
    private final TaskRepository taskRepository;
    private final UsersTasksRepository usersTasksRepository;
    private final RoleRepository roleRepository;

    public UserTaskService(TaskRepository taskRepository, UsersTasksRepository usersTasksRepository, RoleRepository roleRepository) {
        this.taskRepository = taskRepository;
        this.usersTasksRepository = usersTasksRepository;
        this.roleRepository = roleRepository;
    }

    public List<UserTasks> addUserTasks(ReportDto reportDto, String role) {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);

        List<UserTasks> userTasksList = new ArrayList<>();

        for (Tasks tasks : taskRepository.findAll()) {
            UserTasks userTasks = new UserTasks(tasks.getId(), tasks.getName(), 0, tasks.getMaxBall(), tasks.getMinBall(), (float) 0);
            if (tasks.isType()) {
                userTasks.setPlan(0);
                userTasks.setImplementation(0);
            }

            userTasks.setRoleId(tasks.getRoleId());
            userTasks.setType(tasks.isType());
            userTasks.setRoleName(tasks.getRoleName());
            userTasksList.add(userTasks);
        }

        for (ForResponseDto task : reportDto.getTasks()) {
            for (UserTasks userTasks : userTasksList) {
                if (userTasks.getTaskId().equals(task.getTaskId())) {
                    if (userTasks.getRoleName().equals(role)) {
                        if (userTasks.isType()) {
                            userTasks.setPlan(task.getPlan());
                            userTasks.setImplementation(task.getImplementation());
                            userTasks.setPercentage(Float.valueOf(df.format((float) (task.getImplementation() * 100) / task.getPlan())));
                            userTasks.setScore(calculate(userTasks.getPercentage()));

                        } else {
                            userTasks.setScore(Integer.parseInt(task.getValue()));
                            userTasks.setPercentage(Float.parseFloat(task.getValue()));
                        }
                    }
                }
                usersTasksRepository.save(userTasks);
            }
        }
        return userTasksList;
    }

    public Integer calculate(Float percentage) {

        int ans = 0;

        if (percentage < 26 && percentage > 0) {
            ans = 10;
        }
        if (percentage < 51 && percentage > 25) {
            ans = 30;
        }
        if (percentage < 76 && percentage > 50) {
            ans = 50;
        }
        if (percentage < 101 && percentage > 75) {
            ans = 70;
        }
        return ans;
    }

    public List<UserTasks> editUserTasks(List<UserTasks> userTasksList, ReportDto reportDto, String role) {

//        if (role.equals("ROLE_Kadr")) {
//
//            for (UserTasks userTasks : userTasksList) {
//                Optional<Tasks> byId = taskRepository.findById(userTasks.getTaskId());
//                System.out.println(byId);
//
//                if (taskRepository.findById(userTasks.getTaskId()).get().getType()) {
//                    editTask(reportDto, userTasks);
//                }
//            }
//            return userTasksList;
//
//        } else if (role.equals("ROLE_Nazoratchi")) {
//            for (UserTasks userTasks : userTasksList) {
//                if (!taskRepository.findById(userTasks.getTaskId()).get().getType()) {
//                    editTask(reportDto, userTasks);
//                }
//            }
//            return userTasksList;
//
//        } else {
//            for (UserTasks userTasks : userTasksList) {
//                editTask(reportDto, userTasks);
//            }
//            return userTasksList;
//
//        }

        Role role1 = roleRepository.findByName(role).get();


        if (!role.equals("ROLE_Admin")) {

            for (UserTasks userTasks : userTasksList) {
                if (userTasks.getRoleId().equals(role1.getId())) {
                    editTask(reportDto, userTasks);
                }
            }

        } else {
            for (UserTasks userTasks : userTasksList) {
                editTask(reportDto, userTasks);
            }
        }

        return userTasksList;
    }

    private void editTask(ReportDto reportDto, UserTasks userTasks) {

        for (ForResponseDto tasks : reportDto.getTasks()) {
            if (tasks.getTaskId().equals(userTasks.getTaskId())) {

                if (userTasks.isType()) {

                    userTasks.setPlan(tasks.getPlan());
                    userTasks.setImplementation(tasks.getImplementation());
                    userTasks.setPercentage((float) ((tasks.getImplementation() * 100) / tasks.getPlan()));
                    userTasks.setScore(calculate(userTasks.getPercentage()));

                } else {
                    userTasks.setScore(Integer.parseInt(tasks.getValue()));
                    userTasks.setPercentage(Float.parseFloat(tasks.getValue()));
                }
                userTasks.setMaxBall(taskRepository.findById(tasks.getTaskId()).get().getMaxBall());
                userTasks.setMinBall(taskRepository.findById(tasks.getTaskId()).get().getMinBall());
                usersTasksRepository.save(userTasks);
            }
        }
    }

}
