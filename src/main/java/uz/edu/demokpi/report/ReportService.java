package uz.edu.demokpi.report;

import org.springframework.stereotype.Service;
import uz.edu.demokpi.categories.*;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.role.Role;
import uz.edu.demokpi.role.RoleRepository;
import uz.edu.demokpi.task.TaskRepository;
import uz.edu.demokpi.task.Tasks;
import uz.edu.demokpi.user.*;

import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


import static uz.edu.demokpi.user.UsersStates.AT_WORK;

@Service
public class ReportService extends AddNullReports implements ParseUser {
    private final UserTaskService userTaskService;
    private final ReportRepository reportRepository;
    private final UsersRepository usersRepository;
    private final UserService userService;
    private final CategoryRepository categoryRepository;
    private final UsersTasksRepository usersTasksRepository;
    private final RoleRepository roleRepository;
    private final CategoryReportRepository categoryReportRepository;
    private final TaskRepository taskRepository;

    public ReportService(UserTaskService userTaskService, ReportRepository reportRepository, UsersRepository usersRepository, UserService userService, CategoryRepository categoryRepository, UsersTasksRepository usersTasksRepository, RoleRepository roleRepository, CategoryReportRepository categoryReportRepository, TaskRepository taskRepository) {
        this.userTaskService = userTaskService;
        this.reportRepository = reportRepository;
        this.usersRepository = usersRepository;
        this.userService = userService;
        this.categoryRepository = categoryRepository;
        this.usersTasksRepository = usersTasksRepository;
        this.roleRepository = roleRepository;
        this.categoryReportRepository = categoryReportRepository;
        this.taskRepository = taskRepository;
    }

    public StateMessage addReport(ReportDto reportDto, String role) {
        for (ForResponseDto task : reportDto.getTasks()) {
            Tasks tasks = taskRepository.findById(task.getTaskId()).get();

            if (task.getValue() != null) {

                if (Integer.parseInt(task.getValue()) > tasks.getMaxBall() || Integer.parseInt(task.getValue()) < tasks.getMinBall()) {
                    return new StateMessage("Ballar noto`g`ri kiritilgan", false);
                }
            }
            if (task.getPlan() != null && task.getImplementation() != null) {
                if (task.getImplementation() > task.getPlan()) {
                    return new StateMessage("Bajarilgan ish rejadan jo`p kiritilgan", false);
                }
            }
        }

        Optional<Report> byMonth = reportRepository.findByUsers_IdAndYearAndMonth(reportDto.getUserId(), Year.now().getValue(), YearMonth.now().getMonthValue());

        if (byMonth.isEmpty()) {

            Report report = new Report();
            report.setUsers(usersRepository.findById(reportDto.getUserId()).get());
            report.setStatus(true);

            report.setUserTasks(userTaskService.addUserTasks(reportDto, role));

            report.setGeneralBall(calculation(report.getUserTasks()));
            report.setAdditionalPayment(calculationPercentage(report.getGeneralBall()));

            Categories categories = report.getUsers().getCategories();
            CategoryReport categoryReport;
            Optional<CategoryReport> byCategoriesAndYearAndMonth = categoryReportRepository.findByCategoriesAndYearAndMonth(categories.getTypeName().equals(TypeCategory.SECTION.getTypeName()) ? categoryRepository.findById(categories.getParentId()).get() : categories, report.getYear() != null ? report.getYear() : Year.now().getValue(), report.getMonth() != null ? report.getMonth() : YearMonth.now().getMonthValue());
            if (byCategoriesAndYearAndMonth.isEmpty()) {
                categoryReport = new CategoryReport();
                categoryReport.setCategories(categories.getTypeName().equals(TypeCategory.SECTION.getTypeName()) ? categoryRepository.findById(categories.getParentId()).get() : categories);
                categoryReportRepository.save(categoryReport);
            } else {
                categoryReport = byCategoriesAndYearAndMonth.get();
            }

            report.setCategoryReport(categoryReport);
            addReportCategory(reportRepository.save(report));

            return new StateMessage("Muvaffaqiyatli qo'shildi", true);

        } else {
            Report report = byMonth.get();

            return editReport(reportDto, role, report.getId());
        }
    }

    public StateMessage editReport(ReportDto reportDto, String role, Integer id) {

        for (ForResponseDto task : reportDto.getTasks()) {
            Tasks tasks = taskRepository.findById(task.getTaskId()).get();

            if (task.getValue() != null) {

                if (Integer.parseInt(task.getValue()) > tasks.getMaxBall() || Integer.parseInt(task.getValue()) < tasks.getMinBall()) {
                    return new StateMessage("Ballar noto`g`ri kiritilgan", false);
                }
            }
            if (task.getPlan() != null && task.getImplementation() != null) {
                if (task.getImplementation() > task.getPlan()) {
                    return new StateMessage("Bajarilgan ish rejadan kjo`p kiritilgan", false);
                }
            }

        }


        Optional<Report> byId = reportRepository.findById(id);
        if (byId.isPresent() && byId.get().isStatus()) {
            Report report = byId.get();
            List<UserTasks> userTasks = userTaskService.editUserTasks(report.getUserTasks(), reportDto, role);

            report.setGeneralBall(calculation(userTasks));
            report.setAdditionalPayment(calculationPercentage(report.getGeneralBall()));

            addReportCategory(reportRepository.save(report));

//            addLeadersAdditionalPayment(report.getUsers().getCategories().getId(), report.getYear(), report.getMonth());

            return new StateMessage("Muvaffaqiyatli o'zgartirildi ", true);

        }
        return new StateMessage("Xatolik", false);
    }

    public Integer calculation(List<UserTasks> userTasks) {
        int result = 0;

        for (UserTasks userTask : userTasks) {
            result += userTask.getScore();

        }
        return result;
    }

    public Integer calculationPercentage(Integer ball) {


        int result = 0;


        if (ball > 49 && ball < 61) {
            result += 100;
        } else if (ball > 60 && ball < 71) {
            result += 150;
        } else if (ball > 70 && ball < 81) {
            result += 200;
        } else if (ball > 80 && ball < 91) {
            result += 250;
        } else if (ball > 90) {
            result += 300;
        }

        return result;
    }

    public StateMessage delete(Integer id, String role) {
        Report report = reportRepository.findById(id).get();

        Role role1 = roleRepository.findByName(role).get();


        List<UserTasks> userTasks = report.getUserTasks();

        for (UserTasks userTask : userTasks) {
            if (!role1.getName().equals("ROLE_Admin")) {
                if (userTask.getRoleId().equals(role1.getId())) {
                    userTask.setScore(0);
                    userTask.setPercentage((float) 0);
                    usersTasksRepository.save(userTask);
                }
            } else {
                userTask.setScore(0);
                userTask.setPercentage((float) 0);
                usersTasksRepository.save(userTask);
            }
        }

        report.setGeneralBall(calculation(report.getUserTasks()));
        report.setAdditionalPayment(calculationPercentage(report.getGeneralBall()));
        reportRepository.save(report);
//        addLeadersAdditionalPayment(report.getUsers().getCategories().getId(), report.getYear(), report.getMonth());


        return new StateMessage("O'chirildi ", true);
    }

    public List<ResponsDTO> getReports(Integer year, Integer month, String userName) {
        Users users1 = usersRepository.findByUsername(userName).get();

        List<Tasks> tasksList = taskRepository.findAll();
        List<ResponsDTO> responsDTOList = new ArrayList<>();

        List<Categories> categoriesList;

        if (users1.getRole().getName().equals("ROLE_O`rinbosar")) {
            categoriesList = categoryRepository.findAllByParentId(users1.getCategories().getId());
        } else {
            categoriesList = categoryRepository.findByTypeName(TypeCategory.BOARDS.getTypeName());
        }


        for (Categories categories : categoriesList) {


            ResponsDTO responsDTO = new ResponsDTO();

            responsDTO.setCategory(pareCategories(categories));
            List<DtoRequest> userAndReportsList = new ArrayList<>();

            for (Integer userId : categories.getUsersId()) {
                Optional<Users> byId = usersRepository.findById(userId);
                if (byId.isPresent()) {
                    Users users = byId.get();
                    if (users.isDeleteUser() && users.getState().equals(AT_WORK.getName())) {
                        Optional<Report> reportOptional = reportRepository.findByUsers_IdAndYearAndMonth(userId, year != null ? year : Year.now().getValue(), month != null ? month : YearMonth.now().getMonthValue());
                        if (users.getLeader() != null && users.getLeader()) {
                            responsDTO.setLeader(parsUser(reportOptional.orElse(addNewReports(tasksList)), users));
                        } else {
                            userAndReportsList.add(parsUser(reportOptional.orElse(addNewReports(tasksList)), users));
                        }
                    }
                }
            }
            responsDTO.setUsers(userAndReportsList);
            responsDTO.setCategoryReport(categoryReportRepository.findByCategoriesAndYearAndMonth(categories, year != null ? year : Year.now().getValue(), month != null ? month : YearMonth.now().getMonthValue()).orElse(null));
            responsDTOList.add(responsDTO);
        }
        return responsDTOList;
    }

    public List<ResponsDTO> getReports2(Integer year, Integer month, String userName) {
        Users users1 = usersRepository.findByUsername(userName).get();

        List<Tasks> tasksList = taskRepository.findAll();
        List<ResponsDTO> responsDTOList = new ArrayList<>();

        List<Categories> categoriesList;

        if (users1.getRole().getName().equals("ROLE_O`rinbosar")) {
            categoriesList = categoryRepository.findAllByParentId(users1.getCategories().getId());
        } else {
            categoriesList = categoryRepository.findByTypeName(TypeCategory.BOARDS.getTypeName());
        }


        for (Categories categories : categoriesList) {


            ResponsDTO responsDTO = new ResponsDTO();

            responsDTO.setCategory(pareCategories(categories));



            List<DtoRequest> userAndReportsList = new ArrayList<>();

            categoryRepository.findAllByParentId(categories.getId());

            for (Integer userId : categories.getUsersId()) {
                Optional<Users> byId = usersRepository.findById(userId);
                if (byId.isPresent()) {
                    Users users = byId.get();
                    if (users.isDeleteUser() && users.getState().equals(AT_WORK.getName())) {
                        Optional<Report> reportOptional = reportRepository.findByUsers_IdAndYearAndMonth(userId, year != null ? year : Year.now().getValue(), month != null ? month : YearMonth.now().getMonthValue());
                        if (users.getLeader() != null && users.getLeader()) {
                            responsDTO.setLeader(parsUser(reportOptional.orElse(addNewReports(tasksList)), users));
                        } else {
                            userAndReportsList.add(parsUser(reportOptional.orElse(addNewReports(tasksList)), users));
                        }
                    }
                }
            }
            responsDTO.setUsers(userAndReportsList);
            responsDTO.setCategoryReport(categoryReportRepository.findByCategoriesAndYearAndMonth(categories, year != null ? year : Year.now().getValue(), month != null ? month : YearMonth.now().getMonthValue()).orElse(null));
            responsDTOList.add(responsDTO);
        }
        return responsDTOList;
    }



    public StateMessage changeStatusReport(ChangeReportDto changeReportDto) {

        boolean ans = false;

        if (changeReportDto.getCategoryId() == null) {

            for (Users users : usersRepository.findAll()) {
                if (!users.getLeader()) {
                    Optional<Report> byReport = reportRepository.findByUsers_IdAndYearAndMonth(users.getId(), changeReportDto.getYear(), changeReportDto.getMonth());
                    if (byReport.isPresent()) {
                        Report report = byReport.get();
                        report.setStatus(changeReportDto.isStatus());
                        reportRepository.save(report);
                    }
                }
            }
        } else {
            Categories categories = categoryRepository.findById(changeReportDto.getCategoryId()).get();

            for (Integer userId : categories.getUsersId()) {
                if (usersRepository.findById(userId).get().getLeader()) {
                    Optional<Report> byReport = reportRepository.findByUsers_IdAndYearAndMonth(userId, changeReportDto.getYear(), changeReportDto.getMonth());
                    if (byReport.isPresent()) {
                        Report report = byReport.get();
                        report.setStatus(changeReportDto.isStatus());
                        reportRepository.save(report);
                    }
                }
            }
        }
        return new StateMessage("Muvaffaqiyatli o`zgartirildi", ans);
    }

    public void addLeadersAdditionalPayment(Integer catecoryId, Integer year, Integer month) {

        Categories categories = categoryRepository.findById(catecoryId).get();

        Integer result = 0;

        int count = 0;

        Users user = null;

        while (true) {

            if (categories.getTypeName().equals(TypeCategory.BOARDS.getTypeName())) {

                for (int usersId : categories.getUsersId()) {


                    Users users = usersRepository.findById(usersId).get();
                    if (!users.getLeader()) {
                        if (users.getState().equals(AT_WORK.getName())) {
                            Optional<Report> byUsers_idAndYearAndMonth = reportRepository.findByUsers_IdAndYearAndMonth(users.getId(), year, month);

                            if (byUsers_idAndYearAndMonth.isPresent()) {
                                Report report = byUsers_idAndYearAndMonth.get();
                                result += report.getAdditionalPayment();

                            }
                            count++;
                        }
                    } else {
                        user = users;
                    }
                }

                if (user != null) {
                    Optional<Report> byUsers_idAndYearAndMonth = reportRepository.findByUsers_IdAndYearAndMonth(user.getId(), year, month);
                    if (byUsers_idAndYearAndMonth.isEmpty()) {

                        Report report = new Report(0, result / count, null, user, true);
                        reportRepository.save(report);

                    } else {
                        Report report = byUsers_idAndYearAndMonth.get();
                        report.setAdditionalPayment(result / count);
                        reportRepository.save(report);
                    }
                }

                break;

            } else {
                categories = categoryRepository.findById(categories.getParentId()).get();
            }
        }
    }

    public List<UserAndReports> parsUser(List<Users> users, Integer year, Integer month) {

        if (year == null) {
            year = Year.now().getValue();
        }
        if (month == null) {
            month = YearMonth.now().getMonthValue();
        }

        List<UserAndReports> userAndReports = new ArrayList<>();

        for (Users user : users) {
            if (user.isDeleteUser()) {

                Optional<Report> by = reportRepository.findByUsers_IdAndYearAndMonth(user.getId(), year, month);
                DtoRequest dtoRequest = userService.parsUser(user);

                userAndReports.add(new UserAndReports(by.isEmpty() ? null : by.get(), dtoRequest));
            }
        }
        return userAndReports;
    }

    public void addReportCategory(Report report1) {
        CategoryReport categoryReport = report1.getCategoryReport();
        List<Report> reportList = reportRepository.findAllByCategoryReportAndYearAndMonth(categoryReport, report1.getYear(), report1.getMonth());
        int generalBall = 0;
        int plan = 0;
        int implementation = 0;
        int count = 0;

        List<UserTasks> userTasksList = new ArrayList<>();

        for (Tasks tasks : taskRepository.findAll()) {
            UserTasks userTasks = new UserTasks(tasks.getId(), tasks.getName(), 0, tasks.getMaxBall(), tasks.getMinBall(), (float) 0);
            if (tasks.isType()) {
                userTasks.setPlan(0);
                userTasks.setImplementation(0);
                userTasks.setPercentage((float) 0);
            }

            userTasks.setRoleId(tasks.getRoleId());
            userTasks.setType(tasks.isType());
            userTasks.setRoleName(tasks.getRoleName());
            usersTasksRepository.save(userTasks);
            userTasksList.add(userTasks);
        }
        categoryReport.setUserTasks(userTasksList);

        for (Report report : reportList) {

            if (report.getUsers().getState().equals(AT_WORK.getName())) {

                for (UserTasks userTask : report.getUserTasks()) {

                    for (UserTasks task : categoryReport.getUserTasks()) {
                        if (Objects.equals(userTask.getTaskId(), task.getTaskId())) {

                            task.setScore((task.getScore() != null ? task.getScore() : 0) + userTask.getScore());
                            if (userTask.isType()) {
                                task.setPlan((task.getPlan() != null ? task.getPlan() : 0) + userTask.getPlan());
                                task.setImplementation((task.getImplementation() != null ? task.getImplementation() : 0) + userTask.getImplementation());
                                if (task.getImplementation() == 0 || task.getPlan() == 0) {
                                    task.setPercentage((float) 0);
                                } else {
                                    task.setPercentage((float) ((task.getImplementation() * 100) / task.getPlan()));
                                }
                                usersTasksRepository.save(task);
                            }
                        }
                    }
                    generalBall += userTask.getScore() != null ? userTask.getScore() : 0;
                    plan += userTask.getPlan() != null ? userTask.getPlan() : 0;
                    implementation += userTask.getImplementation() != null ? userTask.getImplementation() : 0;
                }
            }
        }

        for (Integer userId : categoryReport.getCategories().getUsersId()) {
            Optional<Users> byId = usersRepository.findById(userId);
            if (byId.isPresent()) {
                Users users = byId.get();
                if (users.getLeader() == null || !users.getLeader()) {
                    if (users.getState().equals(AT_WORK.getName())) {
                        count++;
                    }
                }
            }
        }

        if (categoryReport.getYear() == Year.now().getValue() && categoryReport.getMonth() == YearMonth.now().getMonthValue()) {
            categoryReport.setCountUsers(count);
        }

        categoryReport.setYear(report1.getYear());
        categoryReport.setMonth(report1.getMonth());
        categoryReport.setPlan(plan);
        categoryReport.setImplementation(implementation);
        categoryReport.setGeneralBall(generalBall / categoryReport.getCountUsers());
        categoryReport.setAdditionalPayment(calculationPercentage(categoryReport.getGeneralBall()));
        CategoryReport save = categoryReportRepository.save(categoryReport);
        Optional<Users> optionalUsers = usersRepository.findByCategoriesAndLeader(save.getCategories(), true);

        if (optionalUsers.isPresent()) {
            Users users1 = optionalUsers.get();
            Optional<Report> optionalReport = reportRepository.findByUsers_IdAndYearAndMonth(users1.getId(), save.getYear(), save.getMonth());

            if (optionalReport.isPresent()) {
                Report report = optionalReport.get();
                report.setAdditionalPayment(save.getAdditionalPayment());
                report.setGeneralBall(save.getGeneralBall());
                reportRepository.save(report);
            } else {
                Report report = new Report();
                report.setYear(save.getYear());
                report.setMonth(save.getMonth());
                report.setAdditionalPayment(save.getAdditionalPayment());
                report.setGeneralBall(save.getGeneralBall());
                report.setUsers(users1);
                reportRepository.save(report);
            }
        }

    }
}