package uz.edu.demokpi.report;

import java.util.List;

public class ReportDto {

    private Integer userId;
    private List<ForResponseDto> tasks;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public List<ForResponseDto> getTasks() {
        return tasks;
    }

    public void setTasks(List<ForResponseDto> tasks) {
        this.tasks = tasks;
    }
}