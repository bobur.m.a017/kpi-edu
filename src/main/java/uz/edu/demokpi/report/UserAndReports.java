package uz.edu.demokpi.report;

import uz.edu.demokpi.user.DtoRequest;

public class UserAndReports {
    private Report report;
    private DtoRequest dtoRequest;

    public UserAndReports(Report report, DtoRequest dtoRequest) {
        this.report = report;
        this.dtoRequest = dtoRequest;
    }

    public UserAndReports() {
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public DtoRequest getDtoRequest() {
        return dtoRequest;
    }

    public void setDtoRequest(DtoRequest dtoRequest) {
        this.dtoRequest = dtoRequest;
    }
}
