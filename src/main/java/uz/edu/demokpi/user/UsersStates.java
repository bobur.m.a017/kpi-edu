package uz.edu.demokpi.user;


public enum UsersStates {

    AT_WORK(1,"Ishlamoqda"),
     ON_VACATION(2,"Ta`tilda");


    private  Integer id;
    private String name;


    UsersStates(int id, String name) {
        this.id=id;
        this.name=name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
