package uz.edu.demokpi.user;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.edu.demokpi.categories.Categories;

import java.util.List;
import java.util.Optional;


public interface UsersRepository extends JpaRepository<Users, Integer> {
    Boolean existsByUsername(String name);

    Optional<Users> findByUsername(String name);

    List<Users> findAllByCategoriesId(Integer id);

    List<Users> findAllByCategoriesIdAndStateOrderByLeader(String state,Integer id);

    Optional<Users> findByCategoriesIdAndStateOrderByLeader(String state,Integer id);

    Optional<Users> findByCategoriesAndProfession_Name(Categories categories, String profession_name);

    Optional<Users> findByCategoriesAndLeader(Categories categories, Boolean leader);

    boolean existsByJshshir(String jshshir);
    List<Users> findAllByLeaderAndCategoriesId(boolean leader,Integer categoryId);
}
