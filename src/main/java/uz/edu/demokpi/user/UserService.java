package uz.edu.demokpi.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.edu.demokpi.categories.*;
import uz.edu.demokpi.config.ApplicationUsernamePasswordAuthenticationFilter;
import uz.edu.demokpi.dto.DTO;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.profession.ProfessionService;
import uz.edu.demokpi.report.ReportRepository;
import uz.edu.demokpi.report.UserTaskService;
import uz.edu.demokpi.role.Role;
import uz.edu.demokpi.role.RoleRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


@Service
public class UserService implements ParseToDtoCategories {
    private final UsersRepository usersRepository;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final CategoryService categoryService;
    private final ProfessionService professionService;
    private final RoleRepository roleRepository;
    private final ReportRepository reportRepository;
    private final CategoryRepository categoryRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserTaskService userTaskService;


    public UserService(UsersRepository usersRepository, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, CategoryService categoryService, ProfessionService professionService, RoleRepository roleRepository, ReportRepository reportRepository, CategoryRepository categoryRepository, PasswordEncoder passwordEncoder, UserTaskService userTaskService) {
        this.usersRepository = usersRepository;
        this.authenticationFilter = authenticationFilter;
        this.categoryService = categoryService;
        this.professionService = professionService;
        this.roleRepository = roleRepository;
        this.reportRepository = reportRepository;
        this.categoryRepository = categoryRepository;
        this.passwordEncoder = passwordEncoder;
        this.userTaskService = userTaskService;
    }

    public ResponsUser signInUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ResponsUser responsUser = authenticationFilter.successfulAuthentication(request, response);
        if (responsUser.getSuccess()) {
            Users users = usersRepository.findByUsername(responsUser.getUsername()).get();
            responsUser.setLastName(users.getLastname() != null ? users.getLastname() : null);
            responsUser.setName(users.getName() != null ? users.getName() : null);
            Optional<Role> byName = roleRepository.findByName(responsUser.getRole());
            responsUser.setId(byName.get().getId());


        }
        return responsUser;
    }

    public StateMessage signUp(AddAdminDTO user) {

        Optional<Users> byId = usersRepository.findById(user.getId());

        if (byId.isEmpty()) {
            return new StateMessage("Bunday xodim mavjud emas", false);
        }
        Users users = byId.get();
        users.setStatus(true);
        users.setUsername(user.getUsername());
        users.setRole(roleRepository.findById(user.getRoleId()).get());
        users.setPassword(passwordEncoder.encode(user.getPassword()));
        usersRepository.save(users);

        return new StateMessage("Jarayon muvofiyaqatli yakunlandi", true);
    }

    public StateMessage addUser(SignUp signUp) {


        boolean exists = usersRepository.existsByJshshir(signUp.getJshshir());

        if (signUp.getLeader()!=null && signUp.getLeader().equals(true)){
//            List<Users> allByCategoriesId = usersRepository.findAllByLeaderAndCategoriesId(true, signUp.getCategoryId());

//            if (allByCategoriesId.size() > 0){
//              return   new  StateMessage("Boshqa boshliq biriktirilgan",false);
//            }
        }

        if (exists) {
            return new StateMessage("Bunday xodim avval ro'yxatdan o'tgan!", false);
        }
        Users save = usersRepository.save(
                new Users(
                        signUp.getName(),
                        signUp.getLastName(),
                        signUp.getFatherName(),
                        signUp.getStatus(),
                        professionService.getOneProfession(signUp.getProfessionId()),
                        categoryService.getOneCategory(signUp.getCategoryId()),
                        true,
                        signUp.getJshshir(),
                        signUp.getLeader()
                )
        );

        categoryService.addUsersId(save, true);


        return new StateMessage("Muvaffaqaiyatli bajarildi", true);

    }

    public StateMessage edit(SignUp signUp, Integer id) {

        Optional<Users> optionalUsers = usersRepository.findById(id);


        if (signUp.getLeader()!=null && signUp.getLeader().equals(true)){
            List<Users> allByCategoriesId = usersRepository.findAllByLeaderAndCategoriesId(true, signUp.getCategoryId());
            if (allByCategoriesId.size() > 0){
                return   new  StateMessage("Boshqa boshliq biriktirilgan",false);
            }
        }

        if (optionalUsers.isPresent()) {
            Users users1 = optionalUsers.get();

            if (signUp.getUsername() != null) {
                users1.setUsername(signUp.getUsername());
            }
            if (signUp.getName() != null) {
                users1.setName(signUp.getName());
            }
            if (signUp.getLastName() != null) {
                users1.setLastname(signUp.getLastName());
            }
            if (signUp.getFatherName() != null) {
                users1.setFatherName(signUp.getFatherName());
            }
            if (signUp.getPassword() != null) {
                users1.setPassword(signUp.getPassword());
            }

            if (signUp.getRoleId() != null) {
                users1.setRole(roleRepository.findById(signUp.getRoleId()).get());
            }
            if (signUp.getCategoryId() != null) {
                categoryService.addUsersId(users1, false);

                users1.setCategories(categoryService.getOneCategory(signUp.getCategoryId()));
                users1.setLeader(false);
                categoryService.addUsersId(users1, true);
            }

            if (signUp.getProfessionId() != null) {
                users1.setLeader(false);
                users1.setProfession(professionService.getOneProfession(signUp.getProfessionId()));
            }
            if (signUp.getStatus() != null) {
                users1.setState(signUp.getStatus());
            }
            if (signUp.getLeader() != null) {
                users1.setLeader(signUp.getLeader());
            }
            if (signUp.getJshshir() != null) {
                if (!users1.getJshshir().equals(signUp.getJshshir())) {

                    boolean exists = usersRepository.existsByJshshir(signUp.getJshshir());

                    if (!exists) {
                        users1.setJshshir(signUp.getJshshir());
                    } else {
                        usersRepository.save(users1);
                        return new StateMessage("Ushbu pasport seriya raqam boshqa xodimga avval kiritilgan", false);
                    }
                }
            }
            usersRepository.save(users1);

            return new StateMessage("muvaffaqaiyatli o'zgartirildi", true);
        }
        return new StateMessage("Bunaqa user topilmadi", false);
    }

    public StateMessage deleteUser(Integer id) {
        Optional<Users> byId = usersRepository.findById(id);
        if (byId.isEmpty()) {
            return new StateMessage("Bunday xodim mavjud emas", true);
        } else {
            Users users = byId.get();
            users.setDeleteUser(false);
            usersRepository.save(users);
            return new StateMessage("Xodim o'chirildi", true);
        }
    }

    public StateMessage deleteAdmin(Integer id) {
        Optional<Users> byId = usersRepository.findById(id);
        StateMessage stateMessage;

        if (byId.isEmpty()) {
            stateMessage = new StateMessage("Bunday admin mavjud emas", false);
        } else {
            Users users = byId.get();
            users.setPassword(null);
            users.setUsername(null);
            users.setRole(null);
            usersRepository.save(users);
            stateMessage = new StateMessage("Xodim muvaffaqiyatli adminlikdan olindi", false);
        }
        return stateMessage;
    }

    public DtoRequest parsUser(Users user) {
        return
                new DtoRequest(
                        user.getId(),
                        user.getName(),
                        user.getLastname(),
                        user.getFatherName(),
                        user.getState(),
                        pareCategories(user.getCategories()),
                        user.getProfession(),
                        user.getRole() != null ? user.getRole().getName() : null,
                        user.getUsername(),
                        user.getJshshir()
                );
    }

    public DTO getUserByCategory(Integer id, ResponsUser responsUser) {

        DTO dto = new DTO();

        if (id == 0 && responsUser.getRole().equals("ROLE_O`rinbosar")) {
            id = 0;
        }
        List<Categories> list = new ArrayList<>();


        List<Categories> allByParentId = categoryRepository.findAllByParentId(id);


        if (id != 0) {
            List<Users> usersList = usersRepository.findAllByCategoriesId(id);
            List<DtoRequest> requestList = new ArrayList<>();
            for (Users users : usersList) {
                if (users.isDeleteUser()) {
                    requestList.add(parsUser(users));
                }
            }
            dto.setUsers(Collections.singletonList(requestList));
        }
        for (Categories categories : allByParentId) {

            if (responsUser.getRole().equals("ROLE_O`rinbosar")) {
                Users users = usersRepository.findByUsername(responsUser.getUsername()).get();
                if (categories.equals(users.getCategories())) {
                    list.add(categories);
                }
            } else {

                list.add(categories);

            }
        }
        dto.setCategory(list);

        return dto;
    }

    public List<DtoRequest> searchUser(String search, String filter, String jshshir, ResponsUser user) {
        List<Categories> byTypeName = null;
        List<Users> usersList = new ArrayList<>();

        if (user.getRole().equals("ROLE_O`rinbosar")) {

            Users users = usersRepository.findByUsername(user.getUsername()).get();

            byTypeName = categoryRepository.findAllByParentId(users.getCategories().getId());

            for (Integer userId : users.getCategories().getUsersId()) {
                Optional<Users> byId = usersRepository.findById(userId);
                if (byId.isPresent())
                    usersList.add(byId.get());
            }

        } else {
            if (filter != null)
                byTypeName = categoryRepository.findByTypeName(parsCategoryType(filter));


            usersList = usersRepository.findAll();
        }

        List<DtoRequest> list = new ArrayList<>();

        if (filter != null) {

            for (Categories categories : byTypeName) {

                for (Users users : usersRepository.findAllByCategoriesId(categories.getId())) {
                    if (users.isDeleteUser()) {
                        if (search != null) {
                            if (users.getName().toLowerCase(Locale.ROOT).startsWith(search.toLowerCase(Locale.ROOT)) || users.getLastname().toLowerCase(Locale.ROOT).startsWith(search.toLowerCase(Locale.ROOT))) {

                                DtoRequest dtoRequest = parsUser(users);
                                list.add(addCategoriesId(users, dtoRequest));
                            }
                        } else {
                            list.add(addCategoriesId(users, parsUser(users)));
                        }
                    }


                }
            }


        } else if (search != null) {
            for (Users users : usersList) {
                if (users.isDeleteUser()) {
                    if (users.getFatherName().toLowerCase(Locale.ROOT).startsWith(search.toLowerCase(Locale.ROOT)) || users.getName().toLowerCase(Locale.ROOT).startsWith(search.toLowerCase(Locale.ROOT)) || users.getLastname().toLowerCase(Locale.ROOT).startsWith(search.toLowerCase(Locale.ROOT))) {
                        list.add(addCategoriesId(users, parsUser(users)));
                    }
                }
            }
        } else {

            for (Users users : usersList) {
                if (users.isDeleteUser())
                    list.add(addCategoriesId(users, parsUser(users)));
            }
        }

        if (jshshir != null) {
            list.removeIf(dtoRequest -> !dtoRequest.getJshshir().startsWith(jshshir));
        }

        return list;
    }

    public Page<DtoRequest> parsPage(List<DtoRequest> list, Pageable pageable) {
        if (list == null) {
            throw new IllegalArgumentException("To create a Page, the list mustn't be null!");
        }

        int startOfPage = pageable.getPageNumber() * pageable.getPageSize();
        if (startOfPage > list.size()) {
            return new PageImpl<>(new ArrayList<>(), pageable, 0);
        }

        int endOfPage = Math.min(startOfPage + pageable.getPageSize(), list.size());
        return new PageImpl<>(list.subList(startOfPage, endOfPage), pageable, list.size());
    }

    public String parsCategoryType(String str) {
        if (str.equals("section"))
            return TypeCategory.SECTION.getTypeName();
        if (str.equals("management"))
            return TypeCategory.MANAGEMENT.getTypeName();
        if (str.equals("boards"))
            return TypeCategory.BOARDS.getTypeName();

        return null;
    }

    public DtoRequest addCategoriesId(Users users, DtoRequest dtoRequest) {
        if (users.getCategories().getTypeName().equals(TypeCategory.SECTION.getTypeName())) {

            dtoRequest.setSectionId(users.getCategories().getId());
            dtoRequest.setBoardId(categoryRepository.findById(users.getCategories().getParentId()).get().getId());
            dtoRequest.setManagerId(categoryRepository.findById(users.getCategories().getParentId()).get().getParentId());

        } else if (users.getCategories().getTypeName().equals(TypeCategory.BOARDS.getTypeName())) {

            dtoRequest.setSectionId(null);
            dtoRequest.setBoardId(users.getCategories().getId());
            dtoRequest.setManagerId(users.getCategories().getParentId());

        } else if (users.getCategories().getTypeName().equals(TypeCategory.MANAGEMENT.getTypeName())) {

            dtoRequest.setSectionId(null);
            dtoRequest.setBoardId(null);
            dtoRequest.setManagerId(users.getCategories().getId());
        }
        return dtoRequest;
    }

}