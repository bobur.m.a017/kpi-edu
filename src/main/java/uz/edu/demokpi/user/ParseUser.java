package uz.edu.demokpi.user;

import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.categories.ParseToDtoCategories;
import uz.edu.demokpi.categories.TypeCategory;
import uz.edu.demokpi.report.Report;

import java.util.List;

import static uz.edu.demokpi.user.UsersStates.AT_WORK;

public interface ParseUser extends ParseToDtoCategories {

    default DtoRequest parsUser(Report report, Users user) {
        return
                new DtoRequest(
                        user.getId(),
                        user.getName(),
                        user.getLastname(),
                        user.getFatherName(),
                        user.getState(),
                        pareCategories(user.getCategories()),
                        user.getProfession(),
                        user.getRole() != null ? user.getRole().getName() : null,
                        user.getUsername(),
                        user.getJshshir(),
                        report
                );
    }

    default Integer categoriesId(List<Categories> categories, Integer categoryId) {
        for (Categories category : categories) {
            if (category.getId().equals(categoryId)) {
                return category.getId();
            }
        }

        return null;

    }

    default Users getLeader(UsersRepository usersRepository, Categories categories) {
        if (categories.getTypeName().equals(TypeCategory.BOARDS.getTypeName())){
            List<Users> allByCategoriesIdAndStateOrderByLeader = usersRepository.findAllByCategoriesIdAndStateOrderByLeader(AT_WORK.getName(), categories.getId());
            if (allByCategoriesIdAndStateOrderByLeader.size() != 0){
                return allByCategoriesIdAndStateOrderByLeader.get(0);
            }
        }
        return null;

    }

    default List<Users> sortUserByCategory(UsersRepository  usersRepository,Categories categories) {
        return  null;
    }

}
