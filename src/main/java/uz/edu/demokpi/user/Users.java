package uz.edu.demokpi.user;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.profession.Profession;
import uz.edu.demokpi.role.Role;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class Users implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String lastname;
    private String fatherName;
    private String state;
    private boolean status;
    private boolean deleteUser;
    private Boolean leader;

    @Column(unique = true)
    private String jshshir;

    @Column(unique = true)
    private String username;
    private String password;

    @ManyToOne
    private Role role;

    @ManyToOne
    private Profession profession;

    @ManyToOne
    private Categories categories;


    public Users(String name, boolean status, String username, String password, Role role) {
        this.name = name;
        this.status = status;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Users(String name, String lastname, String fatherName, String state, Profession profession, Categories categories,boolean deleteUser,String jshshir,Boolean leader) {
        this.name = name;
        this.lastname = lastname;
        this.fatherName = fatherName;
        this.state = state;
        this.profession = profession;
        this.categories = categories;
        this.deleteUser = deleteUser;
        this.jshshir = jshshir;
        this.leader = leader;
    }

    public Users(Integer id, String name, String lastname, String fatherName, String state, boolean status, String username, String password, Role role, Profession profession, Categories categories, Boolean deleteUser,String jshshir,Boolean leader) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.fatherName = fatherName;
        this.state = state;
        this.status = status;
        this.username = username;
        this.password = password;
        this.role = role;
        this.profession = profession;
        this.categories = categories;
        this.deleteUser = deleteUser;
        this.jshshir = jshshir;
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", state='" + state + '\'' +
                ", status=" + status +
                ", deleteUser=" + deleteUser +
                ", leader=" + leader +
                ", jshshir='" + jshshir + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", profession=" + profession +
                ", categories=" + categories +
                '}';
    }

    public Boolean getLeader() {
        return leader;
    }

    public void setLeader(Boolean leader) {
        this.leader = leader;
    }

    public String getJshshir() {
        return jshshir;
    }

    public void setJshshir(String jshshir) {
        this.jshshir = jshshir;
    }

    public boolean isDeleteUser() {
        return deleteUser;
    }

    public void setDeleteUser(boolean deleteUser) {
        this.deleteUser = deleteUser;
    }



    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return List.of(this.role);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status;
    }

    public Users() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }
}
