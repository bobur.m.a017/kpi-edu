package uz.edu.demokpi.user;

import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.categories.CategoriesResponseDto;
import uz.edu.demokpi.profession.Profession;
import uz.edu.demokpi.report.Report;
import uz.edu.demokpi.role.Role;

import java.util.List;

public class DtoRequest {
    private Integer id;
    private String name;
    private String lastName;
    private String fatherName;
    private String status;
    private CategoriesResponseDto categoryName;
    private Profession professionName;
    private String roleName;
    private String username;
    private String jshshir;
    private Report report;
    private Integer managerId;
    private Integer boardId;
    private Integer sectionId;

    public DtoRequest(Integer id, String name, String lastName, String fatherName, String status, CategoriesResponseDto categoryName, Profession professionName, String roleName, String username,String jshshir) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.status = status;
        this.categoryName = categoryName;
        this.professionName = professionName;
        this.roleName = roleName;
        this.username = username;
        this.jshshir = jshshir;
    }

    public DtoRequest(Integer id, String name, String lastName, String fatherName, String status, CategoriesResponseDto categoryName, Profession professionName, String roleName, String username, String jshshir, Report report) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.status = status;
        this.categoryName = categoryName;
        this.professionName = professionName;
        this.roleName = roleName;
        this.username = username;
        this.jshshir = jshshir;
        this.report = report;
    }

    public DtoRequest(Integer id, String name, String lastName, String fatherName, String status, CategoriesResponseDto categoryName, Profession professionName, String roleName, String username, String jshshir, Report report, Integer managerId, Integer boardId, Integer sectionId) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.status = status;
        this.categoryName = categoryName;
        this.professionName = professionName;
        this.roleName = roleName;
        this.username = username;
        this.jshshir = jshshir;
        this.report = report;
        this.managerId = managerId;
        this.boardId = boardId;
        this.sectionId = sectionId;
    }

    public DtoRequest() {
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CategoriesResponseDto getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(CategoriesResponseDto categoryName) {
        this.categoryName = categoryName;
    }

    public Profession getProfessionName() {
        return professionName;
    }

    public void setProfessionName(Profession professionName) {
        this.professionName = professionName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJshshir() {
        return jshshir;
    }

    public void setJshshir(String jshshir) {
        this.jshshir = jshshir;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }
}
