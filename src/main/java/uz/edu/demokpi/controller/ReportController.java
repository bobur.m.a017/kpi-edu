package uz.edu.demokpi.controller;


import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.edu.demokpi.config.ApplicationUsernamePasswordAuthenticationFilter;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.report.*;
import uz.edu.demokpi.user.ResponsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;

@RestController
@RequestMapping("/kpi/api/report")
public class ReportController {

    private final ReportService reportService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;

    public ReportController(ReportService reportService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter) {
        this.reportService = reportService;
        this.authenticationFilter = authenticationFilter;
    }

    @PreAuthorize("hasAnyRole('Admin','Nazorat','O`rinbosar')")
    @PostMapping
    public ResponseEntity<StateMessage> add(@RequestBody ReportDto reportDto, HttpServletRequest request) throws ServletException, IOException {

//        try {
        ResponsUser responsUser = authenticationFilter.parseToken(request);
        StateMessage stateMessage = reportService.addReport(reportDto, responsUser.getRole());
        return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(), false));
//
//        }
    }

    @PreAuthorize("hasAnyRole('Admin','Nazorat','O`rinbosar')")
    @PutMapping("/{id}")
    public ResponseEntity<StateMessage> edit(@PathVariable Integer id, @RequestBody ReportDto reportDto, HttpServletRequest request) throws ServletException, IOException {

//        try {
        ResponsUser responsUser = authenticationFilter.parseToken(request);
        StateMessage stateMessage = reportService.editReport(reportDto, responsUser.getRole(), id);
        return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);
//
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(), false));
//        }
    }

    @PreAuthorize("hasAnyRole('Admin','Nazorat','O`rinbosar')")
    @DeleteMapping("/{id}")
    public ResponseEntity<StateMessage> delete(@PathVariable Integer id, HttpServletRequest request) {

        try {
            ResponsUser responsUser = authenticationFilter.parseToken(request);
            StateMessage stateMessage = reportService.delete(id, responsUser.getRole());
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(), false));

        }
    }


    @PreAuthorize("hasAnyRole('Admin','O`rinbosar','Bugalter','Vazir','Nazorat')")
    @GetMapping
    public ResponseEntity<?> get(@Param(value = "year") Integer year, @Param(value = "month") Integer month, HttpServletRequest request) {

//        try {
        if (year != null && year > Year.now().getValue() && month != null && month > YearMonth.now().getMonthValue()) {
            return ResponseEntity.ok().body(new ResponsDTO());
        }
        ResponsUser responsUser = authenticationFilter.parseToken(request);
        List<ResponsDTO> reports = reportService.getReports(year, month, responsUser.getUsername());
        return ResponseEntity.ok().body(reports);

//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(), false));
//        }
    }


    @PreAuthorize("hasAnyRole('Admin')")
    @PostMapping("/changeStatusReport")
    public ResponseEntity<?> changeTheStatusOfTheReport(@RequestBody ChangeReportDto reportDto) {
        try {
            StateMessage stateMessage = reportService.changeStatusReport(reportDto);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(), false));
        }
    }
}
