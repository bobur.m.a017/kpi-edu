package uz.edu.demokpi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.categories.CategoryRepository;
import uz.edu.demokpi.categories.CategoryService;
import uz.edu.demokpi.categories.DtoCategories;
import uz.edu.demokpi.dto.StateMessage;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/kpi/api/categories")
public class CategoriesController {
    private final CategoryService categoryService;
    private final CategoryRepository categoryRepository;

    public CategoriesController(CategoryService categoryService, CategoryRepository categoryRepository) {
        this.categoryService = categoryService;
        this.categoryRepository = categoryRepository;
    }


    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping("/getParent/{id}")
    public Categories getParentCategories(@PathVariable Integer id){

        Optional<Categories> byId = categoryRepository.findById(id);

        if (byId.isPresent()){
            Optional<Categories> byId1 = categoryRepository.findById(byId.get().getParentId());
            return byId1.orElse(null);
        }
        return null;
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getAllByNameCategories(@PathVariable Integer id){

        try {
            Categories allCategorybYnAME = categoryService.getAllCategorybYnAME(id);
            return ResponseEntity.ok().body(allCategorybYnAME);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping
    public ResponseEntity<?> getAllCategories(){
        try {
            List<Categories> allCategory = categoryService.getAllCategory();
            return ResponseEntity.ok().body(allCategory);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PostMapping
    public ResponseEntity<StateMessage> addCategories(@RequestBody DtoCategories categories){
        try {
            StateMessage stateMessage = categoryService.addCategories(categories.getName(), categories.getParentId());
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @DeleteMapping("/{id}")
    public ResponseEntity<StateMessage> deleteCategories(@PathVariable Integer id){
        try {
            StateMessage stateMessage = categoryService.deleteCategory(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PutMapping("/{id}")
    public ResponseEntity<StateMessage> editCategories(@PathVariable Integer id, @RequestBody DtoCategories categories){

        try {
            StateMessage stateMessage = categoryService.editCategory(id,categories);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }


    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping("/getSub/{id}")
    public ResponseEntity<?> getSubCategories(@PathVariable Object id){
        try {
            List<Categories> subCategory = categoryService.getSubCategory(id);
            return ResponseEntity.ok().body(subCategory);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));
        }
    }

}
