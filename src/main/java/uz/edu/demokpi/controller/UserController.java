package uz.edu.demokpi.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.edu.demokpi.categories.CategoryService;
import uz.edu.demokpi.config.ApplicationUsernamePasswordAuthenticationFilter;
import uz.edu.demokpi.dto.DTO;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.role.Role;
import uz.edu.demokpi.role.RoleRepository;
import uz.edu.demokpi.user.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/kpi/api/user")
public class UserController {

    private final RoleRepository roleRepository;
    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final ApplicationUsernamePasswordAuthenticationFilter authenticationFilter;
    private final CategoryService categoryService;

    public UserController(RoleRepository roleRepository, UsersRepository usersRepository, PasswordEncoder passwordEncoder, UserService userService, ApplicationUsernamePasswordAuthenticationFilter authenticationFilter, CategoryService categoryService) {
        this.roleRepository = roleRepository;
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.authenticationFilter = authenticationFilter;
        this.categoryService = categoryService;
    }


    @PostMapping("/signIn")
    public ResponseEntity<?> signIn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        for (Users users : usersRepository.findAll()) {
//            if (users.isDeleteUser()) {
//                categoryService.addUsersId(users, true);
//            }
//        }

//        try {
            ResponsUser responsUser = userService.signInUser(request, response);
            return ResponseEntity.status(responsUser.getSuccess()? 200 : 500).body(responsUser);
//        }catch (Exception e){
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage("Qandaydir xatolik qayta uruning",false));
//        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PostMapping
    public ResponseEntity<?> signUp(@RequestBody SignUp signUp)  {
        if (signUp.getSectionId()!=null){
            signUp.setCategoryId(signUp.getSectionId());
        }else if (signUp.getBoardId()!=null){
            signUp.setCategoryId(signUp.getBoardId());
        }else if (signUp.getManagerId()!=null){
            signUp.setCategoryId(signUp.getManagerId());
        }
        StateMessage stateMessage = userService.addUser(signUp);
        return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(stateMessage);
//
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editUser(@PathVariable Integer id, @RequestBody SignUp signUp) throws Exception {
        try {
            if (signUp.getSectionId()!=null){
                signUp.setCategoryId(signUp.getSectionId());
            }else if (signUp.getBoardId()!=null){
                signUp.setCategoryId(signUp.getBoardId());
            }else if (signUp.getManagerId()!=null){
                signUp.setCategoryId(signUp.getManagerId());
            }
            StateMessage edit = userService.edit(signUp, id);
            return ResponseEntity.status(edit.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(edit);
        }catch (Exception e){
            return ResponseEntity.status(500).body(new StateMessage(e.getMessage(),false));
        }

    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> allUsers(@PathVariable Integer id) {
        StateMessage stateMessage = userService.deleteUser(id);
        return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);
    }

    @PreAuthorize("hasAnyRole('Admin')")
    @PostMapping("/signUp")
    public ResponseEntity<?> signUpAdmin(@RequestBody AddAdminDTO adminDTO) {
        try {
            StateMessage stateMessage = userService.signUp(adminDTO);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Amalga oshmadi");

        }
    }

    @PreAuthorize("hasAnyRole('Admin')")
    @DeleteMapping("/deleteAdmin/{id}")
    public ResponseEntity<?> deleteAdmin(@PathVariable Integer id) throws Exception{
        try {
            StateMessage stateMessage = userService.deleteAdmin(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping("/{categoryId}")
    public DTO getCategories(@PathVariable Integer categoryId, HttpServletRequest request) {
        ResponsUser responsUser = authenticationFilter.parseToken(request);
        return userService.getUserByCategory(categoryId, responsUser);
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping
    public DTO getCategories(HttpServletRequest request) {

        ResponsUser responsUser = authenticationFilter.parseToken(request);

        return userService.getUserByCategory(0, responsUser);
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr','O`rinbosar')")
    @GetMapping("/getUser")
    public List<DtoRequest> getAllUser(@Param("search") String search, @Param("filter") String filter,@Param("jshshir") String jshshir, HttpServletRequest request) {

        ResponsUser responsUser = authenticationFilter.parseToken(request);

        return (userService.searchUser(search, filter,jshshir,responsUser));
    }



    public void insertRole() {

        if (!roleRepository.existsByName("ROLE_Admin")) {
            roleRepository.save(new Role("ROLE_Admin"));
        }

        if (!roleRepository.existsByName("ROLE_Kadr")) {
            roleRepository.save(new Role("ROLE_Kadr"));
        }

        if (!roleRepository.existsByName("ROLE_Buxgalter")) {
            roleRepository.save(new Role("ROLE_Buxgalter"));
        }

        if (!roleRepository.existsByName("ROLE_Nazorat")) {
            roleRepository.save(new Role("ROLE_Nazorat"));
        }

        if (!roleRepository.existsByName("ROLE_Vazir")) {
            roleRepository.save(new Role("ROLE_Vazir"));
        }

        if (!roleRepository.existsByName("ROLE_O`rinbosar")) {
            roleRepository.save(new Role("ROLE_O`rinbosar"));
        }
    }

    public void insertAdmin() {
        if (!usersRepository.existsByUsername("admin")) {
            usersRepository.save(new Users(
                    "Bobur",
                    true,
                    "admin",
                    passwordEncoder.encode("admin"),
                    roleRepository.findByName("ROLE_Admin").get()
            ));
        }
//        Users admin = usersRepository.findByUsername("admin").get();
//
//        admin.setPassword(passwordEncoder.encode("admin"));
//        usersRepository.save(admin);

    }

    public void insertProfession() {
    }




}
























