package uz.edu.demokpi.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.role.DtoRequestRole;
import uz.edu.demokpi.role.RoleService;
import uz.edu.demokpi.task.Tasks;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/kpi/api/role")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;

    }

    @PreAuthorize("hasAnyRole('Admin')")
    @GetMapping
    public ResponseEntity<?> getAllRole(){

        try {
            List<DtoRequestRole> allRole = roleService.getAllRole();
            return ResponseEntity.status(HttpStatus.OK).body(allRole);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }

//    @PreAuthorize("hasAnyRole('Admin')")
//    @PostMapping
//    public StateMessage addAndEdit(@RequestBody DtoRequestRole dtoRequestRole){
//        return roleService.add(dtoRequestRole);
//    }

//    @PreAuthorize("hasAnyRole('Admin')")
//    @DeleteMapping("/{id}")
//    public StateMessage delete(@PathVariable Integer id){
//        return roleService.deleteRole(id);
//    }
}
