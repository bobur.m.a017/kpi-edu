package uz.edu.demokpi.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.task.TaskDTO;
import uz.edu.demokpi.task.TaskService;
import uz.edu.demokpi.task.Tasks;

import java.util.List;


@RestController
@RequestMapping("/kpi/api/tasks")
public class TaskController {

    private final TaskService taskServise;

    public TaskController(TaskService taskServise) {
        this.taskServise = taskServise;
    }


    @PreAuthorize("hasAnyRole('Admin')")
    @PostMapping
    public ResponseEntity<StateMessage> add(@RequestBody TaskDTO taskDTO ){
        try {
            StateMessage add = taskServise.add(taskDTO);
            return ResponseEntity.status(add.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(new StateMessage("q'shildi",true,add));
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }


    @PreAuthorize("hasAnyRole('Admin')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id){
        try {
            StateMessage delete =taskServise.delete(id);;
            return ResponseEntity.status(delete.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(delete);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }


    @PreAuthorize("hasAnyRole('Admin')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable Integer id, @RequestBody TaskDTO taskDTO){
        try {
            StateMessage edit = taskServise.edit(taskDTO, id);
            return ResponseEntity.status(edit.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(edit);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }


    @PreAuthorize("hasAnyRole('Admin','O`rinbosar','Nazorat')")
    @GetMapping
    public ResponseEntity<?> get(){
        try {
            List<Tasks> get = taskServise.getAll();
            return ResponseEntity.status(HttpStatus.OK).body(get);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }


    @PreAuthorize("hasAnyRole('Admin')")
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Integer id){

        try {
            Tasks tasks = taskServise.get(id);
            return ResponseEntity.status(HttpStatus.OK).body(tasks);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }
}
