package uz.edu.demokpi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.edu.demokpi.categories.Categories;
import uz.edu.demokpi.categories.DtoCategories;
import uz.edu.demokpi.dto.StateMessage;
import uz.edu.demokpi.profession.Profession;
import uz.edu.demokpi.profession.ProfessionDTO;
import uz.edu.demokpi.profession.ProfessionService;

import java.util.List;

@RestController
@RequestMapping("/kpi/api/profession")
public class ProfessionController {
    private  final ProfessionService professionService;

    public ProfessionController(ProfessionService professionService) {
        this.professionService = professionService;
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @GetMapping
    public ResponseEntity<?> getAllProfession(){

        try {
            List<Profession> allProfessions = professionService.getAllProfessions();
            return ResponseEntity.ok().body(allProfessions);
        }catch (Exception e){
            return ResponseEntity.status(417).body(new StateMessage(e.getMessage(),false));
        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PostMapping
    public ResponseEntity<?> addProfession(@RequestBody ProfessionDTO categories){

        try {
            StateMessage stateMessage = professionService.addProfession(categories.getName());
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProfession(@PathVariable Integer id){
        try {
            StateMessage stateMessage = professionService.deleteProfession(id);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }

    @PreAuthorize("hasAnyRole('Admin','Kadr')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editProfession(@PathVariable Integer id, @RequestBody ProfessionDTO professionDTO){

        try {
            StateMessage stateMessage = professionService.editProfession(professionDTO,id);
            return ResponseEntity.status(stateMessage.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(stateMessage);

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new StateMessage(e.getMessage(),false));

        }
    }
}
