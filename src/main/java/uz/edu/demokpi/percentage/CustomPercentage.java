package uz.edu.demokpi.percentage;

import org.springframework.data.rest.core.config.Projection;

@Projection(types = Percentage.class)
public interface CustomPercentage {
    Integer getId();
    Integer getPercentage();
}
