package uz.edu.demokpi.percentage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
@PreAuthorize("hasAnyRole('Admin','Buxgalter')")
@RepositoryRestResource(path = "percentage",excerptProjection = CustomPercentage.class)
public interface PercentageRepository extends JpaRepository<Percentage,Integer> {

}
